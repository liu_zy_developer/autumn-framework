package com.example.autumn.jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author liuzhiyong
 * @date 2023/11/3
 * description:
 */
@FunctionalInterface
public interface PreparedStatementCallback<T> {

    /**
     * 使用预编译对象的操作数据库
     *
     * @param ps 预编译的SQL语句对象, 用于执行带有参数的SQL查询或者更新
     * @return {@link T } 结果
     * @author liuzhiyong
     * @date 2023/11/3
     */
    T doInPreparedStatement(PreparedStatement ps) throws SQLException;

}
