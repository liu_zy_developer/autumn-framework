package com.example.autumn.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author liuzhiyong
 * @date 2023/11/7
 * description:
 */
public interface ResultSetExtractor<T> {

    T extractData(ResultSet rs) throws SQLException;

}
