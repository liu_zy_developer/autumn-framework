package com.example.autumn.jdbc.tx;

import com.example.autumn.exeception.TransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author liuzhiyong
 * @date 2023/11/7
 * description:
 */
public class DataSourceTransactionManager implements PlatformTransactionManager, InvocationHandler {

    final Logger logger = LoggerFactory.getLogger(getClass());

    static final ThreadLocal<TransactionStatus> transactionStatus = new ThreadLocal<>();
    final DataSource dataSource;

    public DataSourceTransactionManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        TransactionStatus ts = transactionStatus.get();
        if (ts == null) {
            // 当前无事务, 开启新的事务
            try (Connection connection = dataSource.getConnection()) {
                final boolean autoCommit = connection.getAutoCommit();
                // 如果开启了自动提交, 关闭自动提交
                if (autoCommit) {
                    connection.setAutoCommit(false);
                }
                try {
                    // 设置线程状态
                    transactionStatus.set(new TransactionStatus(connection));
                    // 调用业务方法
                    Object r = method.invoke(proxy, args);
                    // 提交事务
                    connection.commit();
                    // 方法返回
                    return r;
                } catch (InvocationTargetException e) {
                    logger.warn("will rollback transaction for caused exception: {}", e.getCause() == null ? "null" : e.getCause().getClass().getName());
                    // 出现异常, 回滚事务
                    TransactionException te = new TransactionException(e.getCause());
                    try {
                        // 回滚
                        connection.rollback();
                    } catch (SQLException sqle) {
                        // 回滚出现异常的话, 追加异常
                        te.addSuppressed(sqle);
                    }
                    throw te;
                } finally {
                    // 删除事务状态
                    transactionStatus.remove();
                    if (autoCommit) {
                        // 如果以前设置了开启了自动提交事务, 开启自动提交事务
                        connection.setAutoCommit(true);
                    }
                }
            }
        } else {
            // 当前已有事务, 加入到当前事务中执行
            return method.invoke(proxy, args);
        }
    }

}
