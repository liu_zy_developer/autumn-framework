package com.example.autumn.jdbc.tx;

import jakarta.annotation.Nullable;

import java.sql.Connection;

/**
 * @author liuzhiyong
 * @date 2023/11/8
 * description: 事务工具类
 */
public class TransactionalUtils {

    /**
     * 获取当前线程事务的数据库连接
     *
     *
     * @return {@link Connection }
     * @author liuzhiyong
     * @date 2023/11/8
     */
    @Nullable
    public static Connection getCurrentConnection() {
        TransactionStatus ts = DataSourceTransactionManager.transactionStatus.get();
        // 如果开启了事务, 返回当前事务的数据库连接 , 如果没有开启事务返回null
        return ts == null ? null : ts.connection;
    }


}
