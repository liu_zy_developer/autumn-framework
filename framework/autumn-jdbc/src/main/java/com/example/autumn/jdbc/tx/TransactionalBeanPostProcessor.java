package com.example.autumn.jdbc.tx;

import com.example.autumn.annoation.Transactional;
import com.example.autumn.aop.AnnotationProxyBeanPostProcessor;

/**
 * @author liuzhiyong
 * @date 2023/11/8
 * description:
 */
public class TransactionalBeanPostProcessor extends AnnotationProxyBeanPostProcessor<Transactional> {
}
