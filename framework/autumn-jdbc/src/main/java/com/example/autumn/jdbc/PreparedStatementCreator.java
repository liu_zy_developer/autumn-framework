package com.example.autumn.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author liuzhiyong
 * @date 2023/11/3
 * description: 函数式接口, 创建预编译对象
 */
@FunctionalInterface
public interface PreparedStatementCreator {

    /**
     * 创建预编译对象
     *
     * @param con 数据库连接
     * @return {@link PreparedStatement } 预编译的SQL语句对象, 用于执行带有参数的SQL查询或者更新
     * @author liuzhiyong
     * @date 2023/11/3
     */
    PreparedStatement createPreparedStatement(Connection con) throws SQLException;

}
