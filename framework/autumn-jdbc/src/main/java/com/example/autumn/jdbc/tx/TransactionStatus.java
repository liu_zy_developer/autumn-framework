package com.example.autumn.jdbc.tx;

import java.sql.Connection;

/**
 * @author liuzhiyong
 * @date 2023/11/7
 * description: 事务状态
 */
public class TransactionStatus {

    final Connection connection;

    public TransactionStatus(Connection connection) {
        this.connection = connection;
    }
}
