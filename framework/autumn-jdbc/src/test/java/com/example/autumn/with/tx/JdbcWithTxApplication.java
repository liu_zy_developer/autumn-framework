package com.example.autumn.with.tx;


import com.example.autumn.annotation.ComponentScan;
import com.example.autumn.annotation.Configuration;
import com.example.autumn.annotation.Import;
import com.example.autumn.jdbc.JdbcConfiguration;

@ComponentScan
@Configuration
@Import(JdbcConfiguration.class)
public class JdbcWithTxApplication {

}
