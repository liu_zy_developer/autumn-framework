package com.example.autumn.annotation;

import com.example.autumn.jdbc.JdbcConfiguration;
import com.example.autumn.web.WebMvcConfiguration;

import java.lang.annotation.*;

/**
 * @author liuzhiyong
 * @date 2023/11/10
 * description:
 */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Configuration
@ComponentScan
//@Import({WebMvcConfiguration.class, JdbcConfiguration.class})
public @interface AutumnBootApplication {

    /**
     * Bean name. Default to simple class name with first-letter-lower-case.
     */
    String value() default "";

}
