package com.example.autumn.scan.proxy;


import com.example.autumn.annotation.Autowired;
import com.example.autumn.annotation.Component;

@Component
public class InjectProxyOnPropertyBean {

    @Autowired
    public OriginBean injected;
}
