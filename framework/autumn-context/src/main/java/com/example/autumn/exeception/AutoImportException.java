package com.example.autumn.exeception;

public class AutoImportException extends NestedRuntimeException {

    public AutoImportException() {
    }

    public AutoImportException(String message) {
        super(message);
    }

    public AutoImportException(String message, Throwable cause) {
        super(message, cause);
    }

    public AutoImportException(Throwable cause) {
        super(cause);
    }
}
