package com.example.autumn.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

/**
 * @author liuzhiyong
 * @date 2023/11/10
 * description: 加载自动装配文件
 */
public class AutumnFactoriesLoader {

    private static final String AUTUMN_AUTO_IMPORT_FILE = "META-INF/autumn/";

    private static final String IMPORT_FILE_NAME = "AutoConfiguration.imports";

    public List<String> loadImport() {
        ResourceResolver resolver = new ResourceResolver(AUTUMN_AUTO_IMPORT_FILE);
        return resolver.scanFile(IMPORT_FILE_NAME);
    }

}
