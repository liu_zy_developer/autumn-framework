package com.example.autumn.aop;

import com.example.autumn.annotation.AroundWithHandler;

/**
 * @author liuzhiyong
 * @date 2023/10/30
 * description: AroundWithHandler注解的后置处理器, 用于生成代理对象, 通用逻辑在父类中
 */
public class AroundProxyBeanPostProcessor extends AnnotationProxyBeanPostProcessor<AroundWithHandler> {
}
