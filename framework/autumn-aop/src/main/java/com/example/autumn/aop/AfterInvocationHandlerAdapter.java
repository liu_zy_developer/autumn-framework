package com.example.autumn.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author liuzhiyong
 * @date 2023/10/30
 * description: 处理器模板方法, 用于定义after的执行
 */
public abstract class AfterInvocationHandlerAdapter implements InvocationHandler {

    /**
     * 抽象函数, 子类需要重写
     *
     * @param proxy 对象
     * @param method 方法
     * @param args 参数
     * @return {@link Object }
     * @author liuzhiyong
     * @date 2023/10/30
     */
    public abstract Object after(Object proxy, Object returnValue, Method method, Object[] args);

    /**
     * 模板方法, 用于定义after执行顺讯, 子类不能重写
     *
     * @param proxy 对象
     * @param method 方法
     * @param args 参数
     * @return {@link Object }
     * @author liuzhiyong
     * @date 2023/10/30
     */
    @Override
    public final Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object ret = method.invoke(proxy, args);
        return after(proxy, ret, method, args);
    }
}
