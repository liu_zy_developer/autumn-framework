package com.example.autumn.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author liuzhiyong
 * @date 2023/10/30
 * description: 处理器模板方法, 用于定义before的执行
 */
public abstract class BeforeInvocationHandlerAdapter implements InvocationHandler {

    /**
     * 抽象函数, 子类需要重写,
     *
     * @param proxy 对象
     * @param method 执行法方法
     * @param args 参数
     * @author liuzhiyong
     * @date 2023/10/30
     */
    public abstract void before(Object proxy, Method method, Object[] args);


    /**
     * 模板方法, 用于定义增强的函数Before执行
     *
     * @param proxy 对象
     * @param method 方法
     * @param args 参数
     * @return {@link Object }
     * @author liuzhiyong
     * @date 2023/10/30
     */
    @Override
    public final Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before(proxy, method, args);
        return method.invoke(proxy, args);
    }
}
