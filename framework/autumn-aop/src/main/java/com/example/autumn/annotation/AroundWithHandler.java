package com.example.autumn.annotation;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface AroundWithHandler {

    /**
     * 调用执行处理的Bean名称.
     */
    String value();

}
