package com.example.autumn.aop.after;

import com.example.autumn.annotation.Component;
import com.example.autumn.aop.AfterInvocationHandlerAdapter;

import java.lang.reflect.Method;

/**
 * @author liuzhiyong
 * @date 2023/10/30
 * description:
 */
@Component
public class PoliteInvocationHandler extends AfterInvocationHandlerAdapter {
    @Override
    public Object after(Object proxy, Object returnValue, Method method, Object[] args) {
        if (returnValue instanceof String s) {
            if (s.endsWith(".")) {
                return s.substring(0, s.length() - 1) + "!";
            }
        }
        return returnValue;
    }
}
