package com.example.autumn.aop.after;

import com.example.autumn.annotation.Bean;
import com.example.autumn.annotation.ComponentScan;
import com.example.autumn.annotation.Configuration;
import com.example.autumn.aop.AroundProxyBeanPostProcessor;

/**
 * @author liuzhiyong
 * @date 2023/10/30
 * description:
 */
@Configuration
@ComponentScan
public class AfterApplication {

    @Bean
    AroundProxyBeanPostProcessor createAroundProxyBeanPostProcessor() {
        return new AroundProxyBeanPostProcessor();
    }

}
