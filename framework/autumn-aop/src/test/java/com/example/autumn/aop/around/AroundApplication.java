package com.example.autumn.aop.around;


import com.example.autumn.annotation.Bean;
import com.example.autumn.annotation.ComponentScan;
import com.example.autumn.annotation.Configuration;
import com.example.autumn.aop.AroundProxyBeanPostProcessor;

@Configuration
@ComponentScan
public class AroundApplication {

    @Bean
    AroundProxyBeanPostProcessor createAroundProxyBeanPostProcessor() {
        return new AroundProxyBeanPostProcessor();
    }
}
