package com.example.autumn.aop.around;


import com.example.autumn.annotation.AroundWithHandler;
import com.example.autumn.annotation.Component;
import com.example.autumn.annotation.Value;

@Component
@AroundWithHandler("aroundInvocationHandler")
public class OriginBean {

    @Value("${customer.name}")
    public String name;

    @Polite
    public String hello() {
        return "Hello, " + name + ".";
    }

    public String morning() {
        return "Morning, " + name + ".";
    }
}
