package com.example.autumn.aop.before;


import com.example.autumn.annotation.Bean;
import com.example.autumn.annotation.ComponentScan;
import com.example.autumn.annotation.Configuration;
import com.example.autumn.aop.AroundProxyBeanPostProcessor;

@Configuration
@ComponentScan
public class BeforeApplication {

    @Bean
    AroundProxyBeanPostProcessor createAroundProxyBeanPostProcessor() {
        return new AroundProxyBeanPostProcessor();
    }
}
