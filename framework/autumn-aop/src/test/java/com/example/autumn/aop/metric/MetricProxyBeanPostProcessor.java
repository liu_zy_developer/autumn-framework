package com.example.autumn.aop.metric;


import com.example.autumn.annotation.Component;
import com.example.autumn.aop.AnnotationProxyBeanPostProcessor;

@Component
public class MetricProxyBeanPostProcessor extends AnnotationProxyBeanPostProcessor<Metric> {

}
