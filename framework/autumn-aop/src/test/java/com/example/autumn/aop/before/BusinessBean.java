package com.example.autumn.aop.before;

import com.example.autumn.annotation.AroundWithHandler;
import com.example.autumn.annotation.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
@AroundWithHandler("logInvocationHandler")
public class BusinessBean {

    final Logger logger = LoggerFactory.getLogger(getClass());

    public String hello(String name) {
        logger.info("Hello, {}.", name);
        return "Hello, " + name + ".";
    }

    public String morning(String name) {
        logger.info("Morning, {}.", name);
        return "Morning, " + name + ".";
    }
}
