package com.example.autumn.aop.after;

import com.example.autumn.annotation.AroundWithHandler;
import com.example.autumn.annotation.Component;

/**
 * @author liuzhiyong
 * @date 2023/10/30
 * description:
 */
@Component
@AroundWithHandler("politeInvocationHandler")
public class GreetingBean {
    public String hello(String name) {
        return "Hello, " + name + ".";
    }

    public String morning(String name) {
        return "Morning, " + name + ".";
    }
}
