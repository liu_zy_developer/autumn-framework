package com.example.autumn.aop.around;


import com.example.autumn.annotation.Autowired;
import com.example.autumn.annotation.Component;
import com.example.autumn.annotation.Order;

@Order(0)
@Component
public class OtherBean {

    public OriginBean origin;

    public OtherBean(@Autowired OriginBean origin) {
        this.origin = origin;
    }
}
