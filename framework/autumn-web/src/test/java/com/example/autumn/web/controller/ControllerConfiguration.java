package com.example.autumn.web.controller;


import com.example.autumn.annotation.Configuration;
import com.example.autumn.annotation.Import;
import com.example.autumn.web.WebMvcConfiguration;

@Configuration
@Import(WebMvcConfiguration.class)
public class ControllerConfiguration {

}
