package com.example.autumn.web;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Map;

/**
 * @author liuzhiyong
 * @date 2023/11/9
 * description: 试图解析器
 */
public interface ViewResolver {

    /**
     * 初始化ViewResolver
     */
    void init();

    /**
     * 渲染
     * @param viewName 视图名称
     * @param model 模型
     * @param req request
     * @param resp response
     */
    void render(String viewName, Map<String, Object> model, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;

}
