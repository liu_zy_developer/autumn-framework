package com.example.autumn.annotation;

import java.lang.annotation.*;

/**
 * @author liuzhiyong
 * @date 2023/11/9
 * description: 控制器注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Controller {

    String value() default "";

}
