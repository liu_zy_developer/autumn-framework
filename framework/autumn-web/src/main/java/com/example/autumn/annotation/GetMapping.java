package com.example.autumn.annotation;

import java.lang.annotation.*;

/**
 * @author liuzhiyong
 * @date 2023/11/9
 * description: Get请求映射
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GetMapping {

    /**
     * 映射的url
     */
    String value();

}
