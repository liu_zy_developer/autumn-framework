package com.example.autumn.annotation;

import java.lang.annotation.*;

/**
 * @author liuzhiyong
 * @date 2023/11/9
 * description: 路径参数
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PathVariable {

    /**
     * 参数名称
     */
    String value();

}
