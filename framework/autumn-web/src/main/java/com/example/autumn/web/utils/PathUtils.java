package com.example.autumn.web.utils;

import jakarta.servlet.ServletException;

import java.util.regex.Pattern;

public class PathUtils {


    /**
     * 将接口路径转成正则
     *
     * @param path 接口路径
     * @return {@link Pattern }
     * @author liuzhiyong
     * @date 2023/11/9
     */
    public static Pattern compile(String path) throws ServletException {
        String regPath = path.replaceAll("\\{([a-zA-Z][a-zA-Z0-9]*)\\}", "(?<$1>[^/]*)");
        if (regPath.indexOf('{') >= 0 || regPath.indexOf('}') >= 0) {
            throw new ServletException("Invalid path: " + path);
        }
        return Pattern.compile("^" + regPath + "$");
    }
}
