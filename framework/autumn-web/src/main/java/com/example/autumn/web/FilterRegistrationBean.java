package com.example.autumn.web;

import jakarta.servlet.Filter;

import java.util.List;

/**
 * @author liuzhiyong
 * @date 2023/11/9
 * description: 过滤器注册Bean
 */
public abstract class FilterRegistrationBean {

    public abstract List<String> getUrlPatterns();

    /**
     * 获取当前类的类名
     * 去除类名结尾的FilterRegistrationBean 和 FilterRegistration
     *
     * 例如:
     * ApiFilterRegistrationBean -> apiFilter
     * ApiFilterRegistration -> apiFilter
     * ApiFilterReg -> apiFilterReg
     *
     * @return {@link String } 小驼峰命名的类名
     * @author liuzhiyong
     * @date 2023/11/9
     */
    public String getName() {
        // 获取类名
        String name = getClass().getSimpleName();
        // 类名首字母改成小写
        name = Character.toLowerCase(name.charAt(0)) + name.substring(1);
        if (name.endsWith("FilterRegistrationBean") && name.length() > "FilterRegistrationBean".length()) {
            return name.substring(0, name.length() - "FilterRegistrationBean".length());
        }
        if (name.endsWith("FilterRegistration") && name.length() > "FilterRegistration".length()) {
            return name.substring(0, name.length() - "FilterRegistration".length());
        }
        return name;
    }

    public abstract Filter getFilter();

}
