package com.example.autumn.annotation;

import java.lang.annotation.*;

/**
 * @author liuzhiyong
 * @date 2023/11/9
 * description: Rest形式的控制器
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface RestController {

    String value() default "";
}
