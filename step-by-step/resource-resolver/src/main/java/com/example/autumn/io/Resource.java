package com.example.autumn.io;

/**
 * @author liuzhiyong
 * @date 2023/10/16
 * @param path 基础包路径
 * @param name 资源名称 例如"org/example/Hello.class"
 * description: 表示文件
 */
public record Resource(String path, String name) {
}
