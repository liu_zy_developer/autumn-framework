package com.example.autumn.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @author liuzhiyong
 * @date 2023/10/16
 * description: 资源解析器, 只负责扫描并列出所有文件, 由客户端决定是找出.class文件, 还是.properties文件
 */
public class ResourceResolver {

    Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 扫描的包路径
     */
    String basePackage;

    public ResourceResolver(String basePackage) {
        this.basePackage = basePackage;
    }

    /**
     * 扫描Resorce资源
     * 扫描指定包下的所有文件
     *
     * @param mapper 映射函数, Resource到ClassName的映射,
     *               Resource是文件资源, 中name属性是 文件路径 例如: "org/example/Hello.class". 需要转成类名,例如: "org.example.Hello"
     *               扫描什么样的文件, 映射成什么样的结果,由调用法自己决定, 例如可以扫描以.class结尾的Resource, 映射成类名, 也可以扫描.properties结尾的Resource
     * @return {@link List<R> }
     * @author liuzhiyong
     * @date 2023/10/16
     */
    public <R> List<R> scan(Function<Resource, R> mapper) throws IOException, URISyntaxException {
        String basePackagePath = this.basePackage.replace(".", "/");
        String path = basePackagePath;
        List<R> collector = new ArrayList<>();
        scan0(basePackagePath, path, collector, mapper);
        return collector;
    }

    /**
     * 扫描文件,将文件构建成资源, 再指定映射函数, 将资源映射成需要的格式
     *
     * @param basePackagePath
     * @param path
     * @param collector
     * @param mapper
     * @return {@link }
     * @author liuzhiyong
     * @date 2023/10/17
     */
    <R> void scan0(String basePackagePath, String path, List<R> collector, Function<Resource,R> mapper) throws IOException, URISyntaxException {
        logger.atDebug().log("scan path: {}", path);
        Enumeration<URL> en = getContextClassLoader().getResources(path);
        while (en.hasMoreElements()) {
            URL url = en.nextElement();
            URI uri = url.toURI();
            String uriStr = removeTrailingSlash(uriToString(uri));
            String uriBaseStr = uriStr.substring(0, uriStr.length() - basePackagePath.length());
            if (uriBaseStr.startsWith("file:")) {
                // 在目录中搜索, 设置扫描的基础uri
                uriBaseStr = uriBaseStr.substring(5);
            }
            if (uriStr.startsWith("jar:")) {
                // 扫描jar包
                scanFile(true, uriBaseStr, jarUriToPath(basePackagePath, uri), collector, mapper);
            } else {
                // 扫描文件
                scanFile(false, uriBaseStr, Paths.get(uri), collector, mapper);
            }
        }
    }


    /**
     * 获取类加载器
     * CLassLoader首先从Thread.currentThread().getContextClassLoader()获取, 获取不到再从当前Class获取
     * 因为Web应用的ClassLoader不是JVM提供的基于Classpath的ClassLoader,
     * 而是Servlet容器提供的CLassLoader, 他不在默认的ClassPath搜索, 而是在/WEB-INF/classes目录和/WEB-INF/lib的所有jar包搜索,
     * 从Thread.currentThread().getContextClassLoader()可以获取到Servlet容器专属的ClassLoader
     *
     *
     * @return {@link ClassLoader }
     * @author liuzhiyong
     * @date 2023/10/16
     */
    ClassLoader getContextClassLoader() {
        ClassLoader cl = null;
        cl = Thread.currentThread().getContextClassLoader();
        if (cl == null) {
            cl = getClass().getClassLoader();
        }
        return cl;
    }

    /**
     * 获取基本包的路径
     *
     * @param basePackagePath 基本包路径
     * @param jarUri  指向jar文件的uri
     * @return {@link Path }
     * @author liuzhiyong
     * @date 2023/10/17
     */
    Path jarUriToPath(String basePackagePath, URI jarUri) throws IOException {
        // Map.of() 空map
        return FileSystems.newFileSystem(jarUri, Map.of()).getPath(basePackagePath);
    }

    /**
     * 扫描文件构建资源集合
     *
     * @param isJar 是否为jar
     * @param base 基础路径
     * @param root 基础路径对象
     * @param collector 资源集合
     * @param mapper 资源映射函数
     * @author liuzhiyong
     * @date 2023/10/17
     */
    <R> void scanFile(boolean isJar, String base, Path root, List<R> collector, Function<Resource, R> mapper) throws IOException {
        String baseDir = removeTrailingSlash(base);
        Files.walk(root).filter(Files::isRegularFile).forEach(file -> {
            Resource res = null;
            if (isJar) {
                res = new Resource(baseDir, removeLeadingSlash(file.toString()));
            } else {
                String path = file.toString();
                String name = removeLeadingSlash(path.substring(baseDir.length()));
                res = new Resource("file:" + path, name);
            }
            logger.atDebug().log("found resource: {}", res);
            // 指定映射函数, 将资源映射成对应的类型
            R r = mapper.apply(res);
            if (r != null) {
                collector.add(r);
            }
        });
    }

    /**
     * uri 转 字符串
     *
     * @param uri
     * @return {@link String }
     * @author liuzhiyong
     * @date 2023/10/16
     */
    String uriToString(URI uri) {
        return URLDecoder.decode(uri.toString(), StandardCharsets.UTF_8);
    }

    /**
     * 删除开头的斜杠
     *
     * @param s
     * @return {@link String }
     * @author liuzhiyong
     * @date 2023/10/16
     */
    String removeLeadingSlash(String s) {
        if (s.startsWith("/") || s.startsWith("\\")) {
            s = s.substring(1);
        }
        return s;
    }

    /**
     * 删除尾部的斜杠
     *
     * @param s
     * @return {@link String }
     * @author liuzhiyong
     * @date 2023/10/16
     */
    String removeTrailingSlash(String s) {
        if (s.endsWith("/") || s.endsWith("\\")) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

}
