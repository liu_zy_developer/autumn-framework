package com.example.autumn.webapp;

import com.example.autumn.annotation.ComponentScan;
import com.example.autumn.annotation.Configuration;

/**
 * @author liuzhiyong
 * @date 2023/11/8
 * description: 类似于SpringBoot启动类
 */
@ComponentScan
@Configuration
public class WebAppConfig {
}
