package com.example.autumn.context;

import jakarta.annotation.Nullable;

import java.util.List;

/**
 * @author liuzhiyong
 * @date 2023/10/25
 * description: Framework级别的代码用的接口
 */
public interface ConfigurableApplicationContext extends ApplicationContext {

    List<BeanDefinition> findBeanDefinitions(Class<?> type);

    @Nullable
    BeanDefinition findBeanDefinition(Class<?> type);

    @Nullable
    BeanDefinition findBeanDefinition(String name);

    @Nullable
    BeanDefinition findBeanDefinition(String name, Class<?> requiredType);

    Object createBeanAsEarlySingleton(BeanDefinition def);

}
