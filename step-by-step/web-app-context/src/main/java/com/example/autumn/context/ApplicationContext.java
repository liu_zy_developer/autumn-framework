package com.example.autumn.context;

import java.util.List;

/**
 * @author liuzhiyong
 * @date 2023/10/25
 * description: 供用户使用的上下文接口
 * AutoCloseable 定义了一个close()方法, 用于关闭资源
 */
public interface ApplicationContext extends AutoCloseable {

    /**
     *是否存在指定name的Bean
     *
     * @param name bean的名称
     * @return {@link boolean } true=存在/fale=不存在
     * @author liuzhiyong
     * @date 2023/10/25
     */
    boolean containsBean(String name);

    /**
     * 根据name返回唯一Bean, 未找到抛出NoSuchBeanDefinitionException
     *
     * @param name bean的名称
     * @return {@link T } Bean对象
     * @author liuzhiyong
     * @date 2023/10/25
     */
    <T> T getBean(String name);

    /**
     * 根据name返回唯一Bean
     * 未找到抛出NoSuchBeanDefinitionException
     * 找到Bean, 但是Type不符合, 抛出BeanNotOfRequiredTypeException
     *
     * @param name bean名称
     * @param requiredType  需要的类型
     * @return {@link T }   bean
     * @author liuzhiyong
     * @date 2023/10/25
     */
    <T> T getBean(String name, Class<T> requiredType);

    /**
     * 根据Type返回唯一的Bean
     * 未找到抛出NoSuchBeanDefinitionException
     *
     * @param requireType 需要的类型
     * @return {@link T } bean
     * @author liuzhiyong
     * @date 2023/10/25
     */
    <T> T getBean(Class<T> requireType);

    /**
     * 根据Type返回一组Bean, 未找到返回空集合
     *
     * @param requireType 类型
     * @return {@link List<T> } bean集合
     * @author liuzhiyong
     * @date 2023/10/25
     */
    <T> List<T> getBeans(Class<T> requireType);

    /**
     * 关闭并执行所有bean的destroy方法
     *
     *
     * @author liuzhiyong
     * @date 2023/10/25
     */
    void close();

}
