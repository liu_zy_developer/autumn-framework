package com.example.autumn.web;

import com.example.autumn.context.ApplicationContext;
import com.example.autumn.io.PropertyResolver;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author liuzhiyong
 * @date 2023/11/8
 * description: 路由解析
 */
public class DispatcherServlet extends HttpServlet {

    final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * IOC 容器
     */
    ApplicationContext applicationContext;

    public DispatcherServlet(ApplicationContext applicationContext,
                             PropertyResolver propertyResolver) {
        this.applicationContext = applicationContext;
    }

    /**
     * 销毁方法
     */
    @Override
    public void destroy() {
        // 销毁IOC容器
        this.applicationContext.close();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter pw = resp.getWriter();
        pw.write("<h1>Hello, world!</h1>");
        pw.flush();
    }

}
