package com.example.autumn.web.utils;

import com.example.autumn.context.ApplicationContextUtils;
import com.example.autumn.io.PropertyResolver;
import com.example.autumn.utils.ClassPathUtils;
import com.example.autumn.utils.YamlUtils;
import com.example.autumn.web.DispatcherServlet;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.UncheckedIOException;
import java.util.Map;
import java.util.Properties;

/**
 * @author liuzhiyong
 * @date 2023/11/8
 * description:
 */
public class WebUtils {

    static final Logger logger = LoggerFactory.getLogger(WebUtils.class);

    static final String CONFIG_APP_YAML = "/application.yml";

    static final String CONFIG_APP_PROP = "/application.properties";

    /**
     * 注册路由服务器, 用于路由解析, 路由转发
     * @param servletContext 上下文
     * @param properyResolver 配置文件解析器
     */
    public static void registerDispatcherServlet(ServletContext servletContext, PropertyResolver properyResolver) {
        // IOC容器初始化完成之后才能获取到ApplicationContext
        DispatcherServlet dispatcherServlet = new DispatcherServlet(ApplicationContextUtils.getRequiredApplicationContext(), properyResolver);
        logger.info("register servlet {} for URL '/'", dispatcherServlet.getClass().getName());
        ServletRegistration.Dynamic dispatcherReg = servletContext.addServlet("dispatcherServlet", dispatcherServlet);
        dispatcherReg.addMapping("/");
        dispatcherReg.setLoadOnStartup(0);
    }

    /**
     * 解析配置文件
     * @return
     */
    public static PropertyResolver createPropertyResolver() {
        final Properties props = new Properties();
        // 加载application.yml
        try {
            Map<String, Object> ymlMap = YamlUtils.loadYamlAsPlainMap(CONFIG_APP_YAML);
            logger.info("load config: {}", CONFIG_APP_YAML);
            for (String key : ymlMap.keySet()) {
                Object value = ymlMap.get(key);
                if (value instanceof String strValue) {
                    props.put(key, strValue);
                }
            }
        } catch (UncheckedIOException e) {
            if (e.getCause() instanceof FileNotFoundException) {
                // 如果yml文件没有找到, 加载properties文件
                ClassPathUtils.readInputStream(CONFIG_APP_PROP, (input) -> {
                    logger.info("load config: {}", CONFIG_APP_PROP);
                    props.load(input);
                    return true;
                });
            }
        }
        return new PropertyResolver(props);
    }

}
