package com.example.autumn.hello;


import com.example.autumn.annotation.ComponentScan;
import com.example.autumn.annotation.Configuration;
import com.example.autumn.annotation.Import;
import com.example.autumn.jdbc.JdbcConfiguration;
import com.example.autumn.web.WebMvcConfiguration;

@ComponentScan
@Configuration
@Import({ JdbcConfiguration.class, WebMvcConfiguration.class })
public class HelloConfiguration {

}
