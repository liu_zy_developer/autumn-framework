package com.example.atumn.scan.circulate;

import com.example.autumn.annotation.Component;

/**
 * @author liuzhiyong
 * @date 2023/10/23
 * description:
 */
@Component
public class TestB {

    private final TestC testC;

    public TestB(TestC testC) {
        this.testC = testC;
    }


    public void testB1() {
        System.out.println("testB1");
    }

}
