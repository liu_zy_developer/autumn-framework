package com.example.atumn.scan;


import com.example.atumn.imported.LocalDateConfiguration;
import com.example.atumn.imported.ZonedDateConfiguration;
import com.example.autumn.annotation.ComponentScan;
import com.example.autumn.annotation.Import;

@ComponentScan
@Import({ LocalDateConfiguration.class, ZonedDateConfiguration.class })
public class ScanApplication {

}
