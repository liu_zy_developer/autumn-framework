package com.example.autumn.scan.proxy;


import com.example.autumn.annotation.Autowired;
import com.example.autumn.annotation.Component;

@Component
public class InjectProxyOnConstructorBean {

    public final OriginBean injected;

    public InjectProxyOnConstructorBean(@Autowired OriginBean injected) {
        this.injected = injected;
    }
}
