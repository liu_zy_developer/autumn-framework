package com.example.autumn.scan.circulate;

import com.example.autumn.annotation.Autowired;
import com.example.autumn.annotation.Component;

/**
 * @author liuzhiyong
 * @date 2023/10/23
 * description:
 */
@Component
public class TestA {

    private final TestB testB;

    public TestA(@Autowired TestB testB) {
        this.testB = testB;
    }

    public void testA1() {
        System.out.println("testA1");
        testB.testB1();
    }

}
