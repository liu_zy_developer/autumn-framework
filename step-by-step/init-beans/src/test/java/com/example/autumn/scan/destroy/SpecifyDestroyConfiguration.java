package com.example.autumn.scan.destroy;


import com.example.autumn.annotation.Bean;
import com.example.autumn.annotation.Configuration;
import com.example.autumn.annotation.Value;

@Configuration
public class SpecifyDestroyConfiguration {

    @Bean(destroyMethod = "destroy")
    SpecifyDestroyBean createSpecifyDestroyBean(@Value("${app.title}") String appTitle) {
        return new SpecifyDestroyBean(appTitle);
    }
}
