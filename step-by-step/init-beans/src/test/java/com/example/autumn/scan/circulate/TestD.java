package com.example.autumn.scan.circulate;

import com.example.autumn.annotation.Component;

/**
 * @author liuzhiyong
 * @date 2023/10/23
 * description:
 */
@Component
public class TestD {

    private final TestA testA;

    private final TestC testC;

    public TestD(TestA testA, TestC testC) {
        this.testA = testA;
        this.testC = testC;
    }

    public void testD1() {
        System.out.println("TestD1");
        testC.testC1();
    }

}
