package com.example.autumn.imported;


import com.example.autumn.annotation.Bean;
import com.example.autumn.annotation.Configuration;

import java.time.ZonedDateTime;

@Configuration
public class ZonedDateConfiguration {

    @Bean
    ZonedDateTime startZonedDateTime() {
        return ZonedDateTime.now();
    }
}
