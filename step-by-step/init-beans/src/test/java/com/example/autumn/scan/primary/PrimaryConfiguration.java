package com.example.autumn.scan.primary;


import com.example.autumn.annotation.Bean;
import com.example.autumn.annotation.Configuration;
import com.example.autumn.annotation.Primary;

@Configuration
public class PrimaryConfiguration {

    @Primary
    @Bean
    DogBean husky() {
        return new DogBean("Husky");
    }

    @Bean
    DogBean teddy() {
        return new DogBean("Teddy");
    }
}
