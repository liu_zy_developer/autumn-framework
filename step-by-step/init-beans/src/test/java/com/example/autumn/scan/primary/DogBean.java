package com.example.autumn.scan.primary;

public class DogBean {

    public final String type;

    public DogBean(String type) {
        this.type = type;
    }
}
