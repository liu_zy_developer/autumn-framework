package com.example.autumn.scan.destroy;

import com.example.autumn.annotation.Component;
import com.example.autumn.annotation.Value;
import jakarta.annotation.PreDestroy;

@Component
public class AnnotationDestroyBean {

    @Value("${app.title}")
    public String appTitle;

    @PreDestroy
    void destroy() {
        this.appTitle = null;
    }
}
