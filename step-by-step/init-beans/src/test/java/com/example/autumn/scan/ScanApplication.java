package com.example.autumn.scan;


import com.example.autumn.annotation.ComponentScan;
import com.example.autumn.annotation.Import;
import com.example.autumn.imported.LocalDateConfiguration;
import com.example.autumn.imported.ZonedDateConfiguration;

@ComponentScan
@Import({ LocalDateConfiguration.class, ZonedDateConfiguration.class })
public class ScanApplication {

}
