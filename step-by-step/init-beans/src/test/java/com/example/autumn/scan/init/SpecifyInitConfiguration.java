package com.example.autumn.scan.init;


import com.example.autumn.annotation.Bean;
import com.example.autumn.annotation.Configuration;
import com.example.autumn.annotation.Value;

@Configuration
public class SpecifyInitConfiguration {

    @Bean(initMethod = "init")
    SpecifyInitBean createSpecifyInitBean(@Value("${app.title}") String appTitle, @Value("${app.version}") String appVersion) {
        return new SpecifyInitBean(appTitle, appVersion);
    }
}
