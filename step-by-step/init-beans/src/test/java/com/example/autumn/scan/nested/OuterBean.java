package com.example.autumn.scan.nested;


import com.example.autumn.annotation.Component;

@Component
public class OuterBean {

    @Component
    public static class NestedBean {

    }
}
