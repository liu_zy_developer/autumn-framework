package com.example.autumn.jdbc;

import jakarta.annotation.Nullable;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author liuzhiyong
 * @date 2023/11/3
 * description: 函数式接口, 以函数的形式操作数据库
 */
@FunctionalInterface
public interface ConnectionCallback<T> {


    /**
     * 在连接内执行操作
     *
     * @param con 数据库连接
     * @return {@link T } 结果
     * @author liuzhiyong
     * @date 2023/11/3
     */
    @Nullable
    T doInConnection(Connection con) throws SQLException;

}
