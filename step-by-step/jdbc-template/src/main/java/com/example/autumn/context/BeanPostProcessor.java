package com.example.autumn.context;

public interface BeanPostProcessor {

    /**
     * Invoked after new Bean().
     * 创建Bean之后, 初始化之前执行
     */
    default Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean;
    }

    /**
     * Invoked after bean.init() called.
     * 在执行了init方法之后执行
     */
    default Object postProcessAfterInitialization(Object bean, String beanName) {
        return bean;
    }

    /**
     * Invoked before bean.setXyz() called.
     * 在set了Bean的属性之后执行
     */
    default Object postProcessOnSetProperty(Object bean, String beanName) {
        return bean;
    }
}
