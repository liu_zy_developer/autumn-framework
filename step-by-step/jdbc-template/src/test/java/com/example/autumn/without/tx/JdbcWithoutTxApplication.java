package com.example.autumn.without.tx;

import com.example.autumn.annotation.*;
import com.example.autumn.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

@ComponentScan
@Configuration
public class JdbcWithoutTxApplication {

    @Bean(destroyMethod = "close")
    DataSource dataSource(
            // properties:
            @Value("${autumn.datasource.url}") String url, //
            @Value("${autumn.datasource.username}") String username, //
            @Value("${autumn.datasource.password}") String password, //
            @Value("${autumn.datasource.driver-class-name:}") String driver, //
            @Value("${autumn.datasource.maximum-pool-size:20}") int maximumPoolSize, //
            @Value("${autumn.datasource.minimum-pool-size:1}") int minimumPoolSize, //
            @Value("${autumn.datasource.connection-timeout:30000}") int connTimeout //
    ) {
        var config = new HikariConfig();
        config.setAutoCommit(false);
        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(password);
        if (driver != null) {
            config.setDriverClassName(driver);
        }
        config.setMaximumPoolSize(maximumPoolSize);
        config.setMinimumIdle(minimumPoolSize);
        config.setConnectionTimeout(connTimeout);
        return new HikariDataSource(config);
    }

    @Bean
    JdbcTemplate jdbcTemplate(@Autowired DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
